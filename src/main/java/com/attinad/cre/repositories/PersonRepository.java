package com.attinad.cre.repositories;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.attinad.cre.graph.entity.Person;

public interface PersonRepository extends GraphRepository<Person> {

}
