package com.attinad.cre.repositories;


import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.attinad.cre.mongo.entity.Event;


public interface EventRepository extends PagingAndSortingRepository<Event, String> {

	List<Event> getEventByEvent(String event);
}
