package com.attinad.cre.repositories;

import java.util.List;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.attinad.cre.graph.entity.Content;

public interface ContentRepository extends GraphRepository<Content> {

	Content findOneByContentId(String contentId);

	List<Content> findByContentId(String userId);
}
