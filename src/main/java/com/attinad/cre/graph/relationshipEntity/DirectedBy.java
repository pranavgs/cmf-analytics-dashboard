package com.attinad.cre.graph.relationshipEntity;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

import com.attinad.cre.graph.entity.Content;
import com.attinad.cre.graph.entity.Person;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
@RelationshipEntity(type = "DIRECTED_BY")
public class DirectedBy {

	@GraphId
	private Long id;
	
	@StartNode
	private Content content;
	
	@EndNode
	private Person director;
	
	public DirectedBy(){
		
	}
	
	public DirectedBy(Content content, Person director){
		this.content = content;
		this.director = director;
		director.addDirectedContent(content);
		content.addDirector(director);
		
	}

	public Long getId() {
		return id;
	}

	public Content getContent() {
		return content;
	}

	public Person getDirector() {
		return director;
	}
	
	
}
