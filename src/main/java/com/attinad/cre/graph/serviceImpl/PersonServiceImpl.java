package com.attinad.cre.graph.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.attinad.cre.graph.entity.Person;
import com.attinad.cre.graph.service.PersonService;
import com.attinad.cre.repositories.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository personRepository;
	@Override
	public Person savePerson(Person person){
		this.personRepository.save(person);
		return person;
	}
}
