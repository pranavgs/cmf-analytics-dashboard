package com.attinad.cre.graph.service;

import org.springframework.stereotype.Service;

import com.attinad.cre.graph.entity.Person;
@Service
public interface PersonService {

	public Person savePerson(Person person);
}
