package com.attinad.cre.graph.entity;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.attinad.cre.graph.relationshipEntity.UserRating;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
@NodeEntity
public class Content {
	
	@GraphId Long id;

	String title;
	String contentId;
	String acceptanceMetrics;
	String certification;
	String contentPricing;
	String criticsChoice;
	String duration;
	String ratingImdb;
	String synopsis;
	long viewedCount;
	long watchedCount;
	float userRatingInAVG;
	
	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getAcceptanceMetrics() {
		return acceptanceMetrics;
	}

	public void setAcceptanceMetrics(String acceptanceMetrics) {
		this.acceptanceMetrics = acceptanceMetrics;
	}

	public String getCertification() {
		return certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public String getContentPricing() {
		return contentPricing;
	}

	public void setContentPricing(String contentPricing) {
		this.contentPricing = contentPricing;
	}

	public String getCriticsChoice() {
		return criticsChoice;
	}

	public void setCriticsChoice(String criticsChoice) {
		this.criticsChoice = criticsChoice;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getRatingImdb() {
		return ratingImdb;
	}

	public void setRatingImdb(String ratingImdb) {
		this.ratingImdb = ratingImdb;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
	
	public ContentProvider getProviders() {
		return providers;
	}

	public void setProviders(ContentProvider providers) {
		this.providers = providers;
	}

	public Release getReleasedOn() {
		return releasedOn;
	}

	public void setReleasedOn(Release releasedOn) {
		this.releasedOn = releasedOn;
	}
	
	public long getViewedCount() {
		return viewedCount;
	}

	public void setViewedCount(long viewedCount) {
		this.viewedCount = viewedCount;
	}

	public long getWatchedCount() {
		return watchedCount;
	}

	public void setWatchedCount(long watchedCount) {
		this.watchedCount = watchedCount;
	}

	public float getUserRatingInAVG() {
		return userRatingInAVG;
	}

	public void setUserRatingInAVG(float userRatingInAVG) {
		this.userRatingInAVG = userRatingInAVG;
	}




	@Relationship(type="DIRECTED_BY", direction = Relationship.OUTGOING)
	Set<Person> directors = new HashSet<>();
	
	public void addDirector(Person director) {
        directors.add(director);
    }
	
	
	@Relationship(type="IN_LANGUAGE", direction = Relationship.OUTGOING)
	Set<Language> languages = new HashSet<>();
	
	public void addLanguage(Language language) {
		languages.add(language);
    }
	
	@Relationship(type="PROVIDER", direction = Relationship.OUTGOING)
	ContentProvider providers;
	
	@Relationship(type="WITH_GENRE", direction = Relationship.OUTGOING)
	Set<Genre> genres = new HashSet<>();
	
	public void addLanguage(Genre genre) {
		genres.add(genre);
    }
	
	@Relationship(type="OF_TYPE", direction = Relationship.OUTGOING)
	ContentType contentType;
	
	@Relationship(type="RELEASED_ON", direction = Relationship.OUTGOING)
	Release releasedOn;

	@Relationship(type="USER_RATING", direction = Relationship.INCOMING)
	Set<UserRating> ratings = new HashSet<>();
	
	public void addRating(UserRating userRating) {
		ratings.add(userRating);
    }
	
	public Long getId() {
		return id;
	}

	public Set<Person> getDirectors() {
		return directors;
	}

	public Set<Language> getLanguages() {
		return languages;
	}

	public Set<Genre> getGenres() {
		return genres;
	}

	public Set<UserRating> getRatings() {
		return ratings;
	}
	
}
