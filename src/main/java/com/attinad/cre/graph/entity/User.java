package com.attinad.cre.graph.entity;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.attinad.cre.graph.relationshipEntity.UserRating;

@NodeEntity
public class User {

	@GraphId Long id;
	
	String name;
	String userId;
	String created;
	String dob;
	String gender;
	String graphApi;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGraphApi() {
		return graphApi;
	}

	public void setGraphApi(String graphApi) {
		this.graphApi = graphApi;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Set<Device> getDevices() {
		return devices;
	}

	public Long getId() {
		return id;
	}

	public Set<Content> getViewedContents() {
		return viewedContents;
	}

	public Set<Content> getWatchedContents() {
		return watchedContents;
	}

	public Set<CellularCarrier> getCellularCarriers() {
		return cellularCarriers;
	}

	public Set<Language> getPreferredLanguage() {
		return preferredLanguage;
	}

	public Set<SubscriptionType> getSubscriptions() {
		return subscriptions;
	}

	public Set<UserRating> getRatings() {
		return ratings;
	}

	@Relationship(type="VIEWED", direction = Relationship.OUTGOING)
	Set<Content> viewedContents = new HashSet<>();
	
	@Relationship(type="WATCHED", direction = Relationship.OUTGOING)
	Set<Content> watchedContents = new HashSet<>();
	
	@Relationship(type="MOBILE_NETWORK", direction = Relationship.OUTGOING)
	Set<CellularCarrier> cellularCarriers = new HashSet<>();
	
	@Relationship(type="FROM", direction = Relationship.OUTGOING)
	Location location;
	
	@Relationship(type="AT_LOCALE", direction = Relationship.OUTGOING)
	Locale locale;
	
	@Relationship(type="PREFERRED_LANGUAGE", direction = Relationship.OUTGOING)
	Set<Language> preferredLanguage = new HashSet<>();
	
	@Relationship(type="USING", direction = Relationship.OUTGOING)
	Set<Device> devices = new HashSet<>();
	
	@Relationship(type="SUBSCRIBED_FOR", direction = Relationship.OUTGOING)
	Set<SubscriptionType> subscriptions = new HashSet<>();
	
	@Relationship(type="USER_RATING", direction = Relationship.OUTGOING)
	Set<UserRating> ratings = new HashSet<>();
	
	public UserRating ratedFor(User user, Content content,long rating,String comment){
		final UserRating userRating = new UserRating(user, content, rating, comment);
		ratings.add(userRating);
		content.addRating(userRating);
		return userRating;
	}
	
}
