package com.attinad.cre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.attinad.cre.util.MongoUtil;
import com.attinad.cre.util.SpringMongoConfig;


@SpringBootApplication
@EnableScheduling
public class SpringBootNeo4jApplication {
	
	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		MongoUtil.mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
		SpringApplication.run(SpringBootNeo4jApplication.class, args);
	}
	public SpringBootNeo4jApplication(){
		
	}
}