package com.attinad.cre.mongo.service.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.attinad.cre.mongo.entity.Event;
import com.attinad.cre.mongo.service.EventProcessingService;
import com.attinad.cre.util.MongoUtil;
@Service
@Repository
public class EventProcessingServiceImpl implements EventProcessingService {

	@Override
	public void processContent() {
//		Query searchUserQuery = new Query(Criteria.where("event").ne("Identify"));
		Query searchUserQuery = new Query(Criteria.where("event").is("ContentClick"));
		List<Event> eventList = MongoUtil.mongoOperation.find(searchUserQuery, Event.class);
		for (Event event : eventList) {
			System.out.println(event.getEvent());
			/***
			 * process and save all nodes related to non Identify event like
			 * 
			 * 
			 */
		}
	}
}
